# Flatterm

This is a demo application to help you create a terminal that can run both
inside and outside of container technologies, specifically Flatpak.

You can re-use fp-vte-util.c to spawn your shell process and it will work both
on the host (for traditional distribution packaging) as well as in a flatpak
container (where org.freedesktop.Flatpak D-Bus service is used).


