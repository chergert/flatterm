/* fp-vte-util.c
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
 * https://www.apache.org/licenses/LICENSE-2.0> or the MIT License
 * <LICENSE-MIT or https://opensource.org/licenses/MIT>, at your
 * option. This file may not be copied, modified, or distributed
 * except according to those terms.
 *
 * SPDX-License-Identifier: (MIT OR Apache-2.0)
 */

#include <vte/vte.h>

#include "fp-vte-util.h"

static void
child_watch_cb (G_GNUC_UNUSED GPid     pid,
                gint                   status,
                G_GNUC_UNUSED gpointer user_data)
{
  g_autoptr(GError) error = NULL;

  if (!g_spawn_check_exit_status (status, &error))
    g_printerr ("%s\n", error->message);

  gtk_main_quit ();
}

static void
spawn_cb (VtePty                 *pty,
          GAsyncResult           *result,
          G_GNUC_UNUSED gpointer  user_data)
{
  g_autoptr(GError) error = NULL;
  GPid child_pid;

  g_assert (VTE_IS_PTY (pty));
  g_assert (G_IS_ASYNC_RESULT (result));

  if (!fp_vte_pty_spawn_finish (pty, result, &child_pid, &error))
    g_error ("Failed to spawn child process: %s", error->message);

  /* Wait for our shell process to complete */
  g_child_watch_add (child_pid, child_watch_cb, NULL);
}

gint
main (gint argc,
      gchar *argv[])
{
  g_autoptr(GError) error = NULL;
  g_autoptr(VtePty) pty = NULL;
  g_autofree gchar *argv0 = NULL;
  gchar *child_argv[2] = { NULL, NULL };
  GtkScrolledWindow *scroller;
  GtkWindow *window;
  VteTerminal *term;

  /* Setup widget tree */
  gtk_init (&argc, &argv);
  window = g_object_new (GTK_TYPE_WINDOW,
                         "default-width", 650,
                         "default-height", 440,
                         "title", "Flatterm",
                         NULL);
  scroller = g_object_new (GTK_TYPE_SCROLLED_WINDOW,
                           "hscrollbar-policy", GTK_POLICY_NEVER,
                           "visible", TRUE,
                           NULL);
  pty = vte_pty_new_sync (fp_vte_pty_default_flags (), NULL, NULL);
  term = g_object_new (VTE_TYPE_TERMINAL,
                       "pty", pty,
                       "visible", TRUE,
                       NULL);
  gtk_container_add (GTK_CONTAINER (window), GTK_WIDGET (scroller));
  gtk_container_add (GTK_CONTAINER (scroller), GTK_WIDGET (term));
  g_signal_connect (window, "delete-event", gtk_main_quit, NULL);

  /* Guess the user shell, possibly from the host */
  if (!(argv0 = fp_vte_guess_shell (NULL, &error)))
    g_error ("Failed to guess shell: %s", error->message);
  child_argv[0] = argv0;

  /* Spawn the shell within the PTY. It will complete asynchronously
   * with a GSubprocess that we can wait on for completion.
   */
  fp_vte_pty_spawn_async (pty,
                          g_get_home_dir (),
                          (const gchar * const *) child_argv,
                          NULL,
                          -1,
                          NULL,
                          (GAsyncReadyCallback) spawn_cb,
                          NULL);

  /* Show the window and block until completion or error */
  gtk_window_present (window);
  gtk_main ();

  return 0;
}
